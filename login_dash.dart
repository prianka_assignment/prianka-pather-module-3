import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'Login Page';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: Scaffold(
        appBar: AppBar(title: const Text(_title)),
        body: const MyStatefulWidget(),
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(10),
                child: const Text(
                  'Login Page',
                  style: TextStyle(
                      color: Colors.blueAccent,
                      fontWeight: FontWeight.w500,
                      fontSize: 30),
                )),
            Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(10),
                child: const Text(
                  'Login using your details',
                  style: TextStyle(fontSize: 20),
                )),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                controller: nameController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'User Name',
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: TextField(
                obscureText: true,
                controller: passwordController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Password',
                ),
              ),
            ),
            TextButton(
              onPressed: () {
              },
              child: const Text('Forgot Password',),
            ),
            Container(
                height: 50,
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: ElevatedButton(
                  child: const Text('Login'),
                  onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => const SecondRoute()),
                      );
                    },
                   )
                ),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const Text('Does not have account?'),
                TextButton(
                  child: const Text(
                    'Register',
                    style: TextStyle(fontSize: 15),
                  ),
                  onPressed: () {
                  },
                )
              ],
            ),
          ],
        ));
  }
}

class SecondRoute extends StatelessWidget {
  const SecondRoute({super.key});

  @override
  Widget build (BuildContext context) => GridView.count(
      primary: false,
      padding: const EdgeInsets.all(20),
      crossAxisSpacing: 10,
      mainAxisSpacing: 10,
      crossAxisCount: 2,
      children: <Widget>[
        Container(
          padding: const EdgeInsets.all(10),
          color: Colors.teal[100],
            child: const Text( 'Profile',
              style: TextStyle(
                  color: Colors.blueAccent,
                  fontWeight: FontWeight.w300,
                  fontSize: 20),

            )),
        Container(
          padding: const EdgeInsets.all(10),
          color: Colors.teal[100],
            child: const Text(
              'Account',
              style: TextStyle(
                  color: Colors.blueAccent,
                  fontWeight: FontWeight.w300,
                  fontSize: 20),
            )),
        Container(
          padding: const EdgeInsets.all(10),
          color: Colors.teal[100],
            child: const Text(
              'Your Activity',
              style: TextStyle(
                  color: Colors.blueAccent,
                  fontWeight: FontWeight.w300,
                  fontSize: 20),
            )),
        Container(
          padding: const EdgeInsets.all(10),
          color: Colors.teal[100],
            child: const Text(
              'Favourites',
              style: TextStyle(
                  color: Colors.blueAccent,
                  fontWeight: FontWeight.w300,
                  fontSize: 20),
            )),
        Container(
          padding: const EdgeInsets.all(10),
          color: Colors.teal[100],
            child: const Text(
              'Orders and payments',
              style: TextStyle(
                  color: Colors.blueAccent,
                  fontWeight: FontWeight.w300,
                  fontSize: 20),
            )),
        Container(
          padding: const EdgeInsets.all(10),
          color: Colors.teal[100],
          child: const Text(
           'Service History',
           style: TextStyle(
            color: Colors.blueAccent,
            fontWeight: FontWeight.w300,
             fontSize: 20),
        )
  ),
      ],
    );
}
